import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.css']
})
export class ListUserComponent implements OnInit {

  title = 'List User';

  settings = {
    columns: {
      username: {
        title: 'Username',
        width : '20%',
        sort : true,
        sortDirection : 'asc',
        filter : true,
        filterFunction : ''
      },
      role: {
        title: 'Role',
        filter: true
      },
      username1: {
        title: 'Username1',
        filter: true
      },
      role1: {
        title: 'Role1',
        filter: true
      }
    }
  };

  data = [
    {username: 'nguyen van A', role: 'admin', username1: 'nguyen van A', role1: 'admin'},
    {username: 'nguyen van B', role: 'admin', username1: 'nguyen van A', role1: 'admin'},
    {username: 'nguyen van C', role: 'admin', username1: 'nguyen van A', role1: 'admin'},
    {username: 'nguyen van D', role: 'admin', username1: 'nguyen van A', role1: 'admin'},
    {username: 'nguyen van E', role: 'admin', username1: 'nguyen van A', role1: 'admin'},
    {username: 'nguyen van E', role: 'admin', username1: 'nguyen van A', role1: 'admin'},
    {username: 'nguyen van E', role: 'admin', username1: 'nguyen van A', role1: 'admin'},
    {username: 'nguyen van E', role: 'admin', username1: 'nguyen van A', role1: 'admin'},
    {username: 'nguyen van E', role: 'admin', username1: 'nguyen van A', role1: 'admin'},
    {username: 'nguyen van E', role: 'admin', username1: 'nguyen van A', role1: 'admin'},
    {username: 'nguyen van E', role: 'admin', username1: 'nguyen van A', role1: 'admin'},
    {username: 'nguyen van E', role: 'admin', username1: 'nguyen van A', role1: 'admin'},
    {username: 'nguyen van E', role: 'admin', username1: 'nguyen van A', role1: 'admin'},
  ];

  constructor() { }

  ngOnInit() {
  }

}
