import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegisterUserComponent } from './register-user/register-user.component';
import { ListUserComponent } from './list-user/list-user.component';
import { StatisticalComponent } from './statistical/statistical.component';
import { LayoutComponent } from './layout/layout.component';

const routes: Routes = [
  { path: '', component: LayoutComponent,
    children:
    [
      { path: '', redirectTo: '/statistical', pathMatch: 'full' },
      { path: 'statistical', component: StatisticalComponent },
      { path: 'user',
        children: [
          {
            path: 'register',
            component: RegisterUserComponent
          },
          {
            path: 'list',
            component: ListUserComponent
          }
        ]
      }
    ]
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
